<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Kategori;
use App\Models\Subkategori;
use App\Models\Walpaper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class Controllers extends Controller
{
    //
    public function getwalpaper($id)
    {
        $kategori       =   Kategori::where('slug', $id)->first();
        $subkategori    =   Subkategori::where('kategori', $kategori->id)->get();
        $data           =   Walpaper::where('kategori', $kategori->id)->get();

        return response()->json(['data' => $data, 'kategori' => $subkategori], 200);
    }

    public function getbykategori($id)
    {
        $data           =   Walpaper::where('subkategori', $id)->get();
        $file = [];
        // foreach($data as $data){
        //     $kategori       =   Kategori::find($data->kategori)->first();
        //     $path = public_path('walpaper/'.$kategori->slug.'/'.$data->gambar);
        //     $file = Storage::url($path);
        // }
        // dump(json_decode(file_get_contents($path), true));exit;

        return response()->json(['data' => $data, 'path' => $file], 200);
    }
}
