<?php

namespace App\Http\Controllers;

use App\Models\Kategori as ModelsKategori;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Kategori extends Controller
{
    public function index()
    {
        $data = ModelsKategori::all();
        return view('kategori', compact('data'));
    }

    public function store(Request $request)
    {
        $data           =   new ModelsKategori;
        $data->nama     =   $request->nama;
        $data->slug     =   str_replace(' ','',strtolower($request->nama));

        $data->save();

        return back();
    }

    public function delete($id)
    {
        $data   =   ModelsKategori::find($id);
        $data->delete();

        return back();
    }
}
