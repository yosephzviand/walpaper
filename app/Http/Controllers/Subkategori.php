<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use App\Models\Subkategori as ModelsSubkategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;

class Subkategori extends Controller
{
    //
    public function index()
    {
        $kategori       =   Kategori::all();
        $data           =   ModelsSubkategori::all();
        return view('subkategori', compact('kategori', 'data'));
    }

    public function store(Request $request)
    {
        if ($request->hasFile('file')) {
            $foto               =   $request->file('file');
            $file               =   time() . '_' . str_replace(" ", "_", $foto->getClientOriginalName());
            $data               =   new ModelsSubkategori;
            $data->kategori     =   $request->kategori;
            $data->subkategori  =   $request->subkategori;
            $data->gambar       =   $file;
            $data->save();

            $tujuan_upload = storage_path('app/public/kategori/');
            $foto->move($tujuan_upload, $file);
        }

        return back();
    }

    public function foto($id)
    {
        $data     =   ModelsSubkategori::find($id);

        $path = storage_path('app/public/kategori/'.$data->gambar);
        $file = File::get($path);

        $type = File::mimeType($path);

        $response = Response::make($file, 200);

        $response->header("Content-Type", $type);

        return $response;
    }

    public function delete($id)
    {
        $data   =   ModelsSubkategori::find($id);
        $data->delete();

        return back();
    }
}
