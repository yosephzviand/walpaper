<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use App\Models\Subkategori;
use App\Models\Walpaper as ModelsWalpaper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;

class Walpaper extends Controller
{
    public function index()
    {
        $kategori   =   Kategori::all();
        $data       =   ModelsWalpaper::all();
        $subkategori    =   Subkategori::all();
        return view('walpaper', compact('kategori','data', 'subkategori'));
    }

    public function store(Request $request)
    {

        if ($request->hasFile('file')) {
            $foto       =   $request->file('file');
            foreach ($foto as $foto) {
                $file               =   time() . '_' . str_replace(" ", "_", $foto->getClientOriginalName());
                $data               =   new ModelsWalpaper;
                $kategori           =   Kategori::find($request->kategori);
                $data->kategori     =   $request->kategori;
                $data->subkategori  =   $request->subkategori;
                $data->gambar       =   $file;
                $data->save();

                $tujuan_upload = storage_path('app/public/walpaper/' . $kategori->slug);
                $foto->move($tujuan_upload, $file);
            }
        }


        return back();
    }

    public function foto($id)
    {
        // dump($id);exit;
        $data     =   ModelsWalpaper::find($id);
        $kategori =   Kategori::find($data->kategori);

        // $path = public_path('walpaper/' . $kategori->slug . '/' . $data->gambar);
        $path = storage_path('app/public/walpaper/' . $kategori->slug . '/' . $data->gambar);
        // dump($path);exit;
        $file = File::get($path);

        $type = File::mimeType($path);

        $response = Response::make($file, 200);

        $response->header("Content-Type", $type);

        return $response;
    }

    public function delete($id)
    {
        $data   =   ModelsWalpaper::find($id);
        $data->delete();

        return back();
    }

    public function kategori(Request $request)
    {
        return Subkategori::where('kategori', $request->kategori)->get();
    }
}
