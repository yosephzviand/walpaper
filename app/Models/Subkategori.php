<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subkategori extends Model
{
    //
    protected $table = 'subkategori';

    use SoftDeletes;

    public function tokategori()
    {
        return $this->belongsTo(Kategori::class,'kategori', 'id');
    }
}
