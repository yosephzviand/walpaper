<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Walpaper extends Model
{
    protected $table = 'walpaper';

    use SoftDeletes;

    public function tokategori()
    {
        return $this->belongsTo(Kategori::class, 'kategori', 'id');
    }

    public function tosubkategori()
    {
        return $this->belongsTo(Subkategori::class, 'subkategori', 'id');
    }
}
