@extends('layouts.admin')

@section('title', 'Dashboard')

@section('content')

    <div class="block-header">
        <div class="row">
            <div class="col-lg-6 col-md-8 col-sm-12">
                <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i
                            class="fa fa-arrow-left"></i></a>Dashboard</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href=""><i class="icon-home"></i></a>
                    </li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ul>
            </div>

        </div>
    </div>

    <div class="row clearfix">
        <div class="col-lg-12    col-md-12">
            <h1 style="text-align: center">Welcome</h1>
        </div>
    </div>
@endsection
