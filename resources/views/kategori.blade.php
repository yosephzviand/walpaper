@extends('layouts.admin')

@section('title', 'Kategori')

@section('content')

    <div class="block-header">
        <div class="row">
            <div class="col-lg-6 col-md-8 col-sm-12">
                <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i
                            class="fa fa-arrow-left"></i></a> Kategori</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href=""><i class="icon-home"></i></a>
                    </li>
                    <li class="breadcrumb-item active">Kategori</li>
                </ul>
            </div>

        </div>
    </div>

    <div class="row clearfix">
        <div class="col-lg-4 col-md-12">
            <div class="card">
                <div class="body">
                    <form action="{{ route('store.kategori') }}" method="POST">
                        @csrf
                        <input type="hidden" name="idedit" value="">
                        <div class="form-group">
                            <label for="">Nama Aplikasi</label>
                            <input type="text" name="nama" class="form-control" id="nama" placeholder="Tuliskan " value=""
                                autocomplete="off" required>
                            @error('nama')
                                <div class="small text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        {{-- <div class="form-group">
                            <label for="">Slug</label>
                            <input type="text" name="slug" class="form-control" id="slug" placeholder="Tuliskan " value="" autocomplete="off" required>
                            @error('slug') <div class="small text-danger">{{ $message }}</div> @enderror
                        </div> --}}
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
        <div class="col-lg-8 col-md-12">
            <div class="card">
                <div class="body">
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-hover js-basic-example dataTable table-custom">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Slug</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data as $i => $val)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            <td>{{ $val->nama }}</td>
                                            <td>{{ $val->slug }}</td>
                                            <td>
                                                {{-- <a href="" class="btn btn-warning btn-sm">Edit</a> --}}
                                                <a href="{{ route('delete.kategori', $val->id) }}" class="btn btn-danger btn-sm">Hapus</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
