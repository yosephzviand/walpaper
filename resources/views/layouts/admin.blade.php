<!doctype html>
<html lang="en">

<head>
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="description" content="Lucid Bootstrap 4.1.1 Admin Template">
    <meta name="author" content="WrapTheme, design by: ThemeMakker.com">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <!-- VENDOR CSS -->
    <link rel="stylesheet" href="{{ asset('lucid') }}/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('lucid') }}/assets/vendor/font-awesome/css/font-awesome.min.css">

    <link rel="stylesheet" href="{{ asset('lucid') }}/assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css">
    <link rel="stylesheet"
        href="{{ asset('lucid') }}/assets/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css">
    <link rel="stylesheet"
        href="{{ asset('lucid') }}/assets/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('lucid') }}/assets/vendor/sweetalert/sweetalert.css" />

    <link rel="stylesheet" href="{{ asset('lucid') }}/assets/vendor/chartist/css/chartist.min.css">
    <link rel="stylesheet"
        href="{{ asset('lucid') }}/assets/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
    <link rel="stylesheet" href="{{ asset('lucid') }}/assets/vendor/toastr/toastr.min.css">
    <link rel="stylesheet" href="{{ asset('lucid') }}/assets/vendor/dropify/css/dropify.min.css">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="{{ asset('lucid/light') }}/assets/css/main.css">
    <link rel="stylesheet" href="{{ asset('lucid/light') }}/assets/css/color_skins.css">
</head>

<body class="theme-cyan">

    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="m-t-30"><img src="{{ asset('lucid/light') }}/assets/images/logo-icon.svg" width="48"
                    height="48" alt="Lucid/light"></div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- Overlay For Sidebars -->

    <div id="wrapper">

        <nav class="navbar navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-btn">
                    <button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu fa fa-bars"></i></button>
                </div>

                <div class="navbar-brand">
                    <a href="#"><img src="{{ asset('lucid') }}/assets/images/logo.svg" alt="Lucid/light Logo"
                            class="img-responsive logo"></a>
                </div>


            </div>
        </nav>

        @include('layouts.sidebar')

        <div id="main-content">
            <div class="container-fluid">
                @yield('content')
            </div>
        </div>

    </div>

    <!-- Javascript -->
    <script src="{{ asset('lucid/light') }}/assets/bundles/libscripts.bundle.js"></script>
    <script src="{{ asset('lucid/light') }}/assets/bundles/vendorscripts.bundle.js"></script>

    <script src="{{ asset('lucid/light') }}/assets/bundles/chartist.bundle.js"></script>
    <script src="{{ asset('lucid/light') }}/assets/bundles/knob.bundle.js"></script>
    <!-- Jquery Knob-->
    <script src="{{ asset('lucid/light') }}/assets/bundles/flotscripts.bundle.js"></script>

    <!-- flot charts Plugin Js -->
    <script src="{{ asset('lucid') }}/assets/vendor/dropify/js/dropify.min.js"></script>
    <script src="{{ asset('lucid') }}/assets/vendor/toastr/toastr.js"></script>
    <script src="{{ asset('lucid') }}/assets/vendor/flot-charts/jquery.flot.selection.js"></script>


    <script src="{{ asset('lucid') }}/assets/vendor/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
    <script src="{{ asset('lucid') }}/assets/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
    <script src="{{ asset('lucid') }}/assets/vendor/jquery-datatable/buttons/buttons.colVis.min.js"></script>
    <script src="{{ asset('lucid') }}/assets/vendor/jquery-datatable/buttons/buttons.html5.min.js"></script>
    <script src="{{ asset('lucid') }}/assets/vendor/jquery-datatable/buttons/buttons.print.min.js"></script>

    <script src="{{ asset('lucid') }}/assets/vendor/sweetalert/sweetalert.min.js"></script>
    <!-- SweetAlert Plugin Js -->

    <script src="{{ asset('lucid/light') }}/assets/bundles/datatablescripts.bundle.js"></script>
    <script src="{{ asset('lucid/light') }}/assets/bundles/mainscripts.bundle.js"></script>
    <script src="{{ asset('lucid/light') }}/assets/js/index.js"></script>
    <script src="{{ asset('lucid/light') }}/assets/js/pages/forms/dropify.js"></script>
    <script src="{{ asset('lucid/light') }}/assets/js/pages/tables/jquery-datatable.js"></script>

    <script>
        $('#kategori').change(function() {
            var kategori = $(this).val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: "{{ route('kategori.walpaper') }}",
                method: "POST",
                data: {
                    kategori: kategori
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    html = '<option value="" disabled selected hidden>Pilih </option>';
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id + '>' + data[i].subkategori +
                            '</option>';
                    }
                    $('#subkategori').html(html);
                }
            });
            return false;
        })
    </script>
</body>

</html>
