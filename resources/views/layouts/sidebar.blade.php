<div id="left-sidebar" class="sidebar">
    <div class="sidebar-scroll">
        <div class="user-account">
            <img src="{{ asset('lucid') }}/assets/images/user.png" class="rounded-circle user-photo"
                alt="User Profile Picture">
            <div class="dropdown">
                <span>Welcome,</span>
                <a href="javascript:void(0);" class="dropdown-toggle user-name" data-toggle="dropdown"><strong>
                        @guest
                        @else
                            {{ Auth::user()->name }}
                        @endguest
                    </strong></a>
                <ul class="dropdown-menu dropdown-menu-right account">
                    {{-- <li><a href="doctor-profile.html"><i class="icon-user"></i>My Profile</a></li>
                    <li><a href="app-inbox.html"><i class="icon-envelope-open"></i>Messages</a></li>
                    <li><a href="javascript:void(0);"><i class="icon-settings"></i>Settings</a></li> --}}
                    {{-- <li class="divider"></li> --}}
                    <li>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                          document.getElementById('logout-form').submit();">
                            <i class="icon-power"></i>Logout</a>
                        </a>
                    </li>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </ul>
            </div>
            <hr>
        </div>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#menu">Menu</a></li>
            {{-- <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#admin">Admin</a></li> --}}
        </ul>

        <!-- Tab panes -->
        <div class="tab-content p-l-0 p-r-0">
            <div class="tab-pane active" id="menu">
                <nav class="sidebar-nav">
                    <ul class="main-menu metismenu">
                        <li class="{{ request()->is('home') ? 'active' : '' }}">
                            <a href="{{ route('home') }}"><i class="icon-home"></i><span>Dashboard</span></a>
                        </li>
                        <li class="{{ request()->is('kategori') ? 'active' : '' }}">
                            <a href="{{ route('index.kategori') }}"><i class="icon-calendar"></i>Nama Aplikasi</a>
                        </li>
                        <li class="{{ request()->is('subkategori') ? 'active' : '' }}">
                            <a href="{{ route('index.subkategori') }}"><i class="icon-list"></i>Kategori</a>
                        </li>
                        <li class="{{ request()->is('walpaper') ? 'active' : '' }}">
                            <a href="{{ route('index.walpaper') }}"><i class="icon-fire"></i>Walpaper</a>
                        </li>
                        {{-- <li class="">
                            <a href=""><i class="icon-list"></i>Galery</a>
                        </li>
                        <li class="">
                            <a href=""><i class="icon-wallet"></i>Payments</a>
                        </li>
                        <li class="">
                            <a href=""><i class="icon-layers"></i>Preview</a>
                        </li>
                        <li class="">
                            <a href=""><i class="icon-bubbles"></i>Invitation</a>
                        </li> --}}
                    </ul>
                </nav>
            </div>
            {{-- <div class="tab-pane" id="admin">
                <nav class="sidebar-nav">
                    <ul class="main-menu metismenu">
                        <li><a href="#"><i class="icon-picture"></i>Design</a></li>
                        <li><a href="#"><i class="icon-control-play"></i>Music</a></li>
                        <li><a href="#"><i class="icon-users"></i>Users</a></li>
                    </ul>
                </nav>
            </div> --}}
        </div>
    </div>
</div>
