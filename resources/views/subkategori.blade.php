@extends('layouts.admin')

@section('title', 'Sub Kategori')

@section('content')

    <div class="block-header">
        <div class="row">
            <div class="col-lg-6 col-md-8 col-sm-12">
                <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i
                            class="fa fa-arrow-left"></i></a>Sub Kategori</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href=""><i class="icon-home"></i></a>
                    </li>
                    <li class="breadcrumb-item active">Sub Kategori</li>
                </ul>
            </div>

        </div>
    </div>

    <div class="row clearfix">
        <div class="col-lg-4 col-md-12">
            <div class="card">
                <div class="body">
                    <form action="{{ route('store.subkategori') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="idedit" value="">
                        <div class="form-group">
                            <label for="">Nama Aplikasi</label>
                            <select name="kategori" class="form-control" id="kategori">
                                <option value="" disabled selected hidden>Pilih </option>
                                @foreach ($kategori as $kat)
                                    <option value="{{ $kat->id }}">{{ $kat->nama }}</option>
                                @endforeach
                            </select>
                            @error('kategori')
                                <div class="small text-danger">{{ message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Sub Kategori</label>
                            <input type="text" name="subkategori" class="form-control" id="subkategori"
                                placeholder="Tuliskan " value="" autocomplete="off" required>
                            @error('subkategori')
                                <div class="small text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <div class="form-group">
                                <input type="file" class="dropify " name="file"
                                    data-allowed-file-extensions="png jpg jpeg">
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
        <div class="col-lg-8 col-md-12">
            <div class="card">
                <div class="body">
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-hover js-basic-example dataTable table-custom">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Aplikasi</th>
                                        <th>Kategori</th>
                                        <th>Gambar</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data as $i => $val)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            <td>{{ $val->tokategori->nama }}</td>
                                            <td>{{ $val->subkategori }}</td>
                                            <td>
                                                <img src="{{ route('foto.subkategori', $val->id ?? '') }}" alt="" title=""
                                                    width="200px">
                                            </td>
                                            <td>
                                                <a href="{{ route('delete.subkategori', $val->id) }}"
                                                    class="btn btn-danger btn-sm">Hapus</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
