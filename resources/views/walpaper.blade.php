@extends('layouts.admin')

@section('title', 'Walpaper')

@section('content')

    <div class="block-header">
        <div class="row">
            <div class="col-lg-6 col-md-8 col-sm-12">
                <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i
                            class="fa fa-arrow-left"></i></a>Walpaper</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href=""><i class="icon-home"></i></a>
                    </li>
                    <li class="breadcrumb-item active">Walpaper</li>
                </ul>
            </div>

        </div>
    </div>

    <div class="row clearfix">
        <div class="col-lg-6 col-md-12">
            <div class="card">
                <div class="body">
                    <form action="{{ route('store.walpaper') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="">Nama Aplikasi</label>
                            <select name="kategori" class="form-control" id="kategori" required>
                                <option value="" disabled selected hidden>Pilih </option>
                                @foreach ($kategori as $kat)
                                    <option value="{{ $kat->id }}">{{ $kat->nama }}</option>
                                @endforeach
                            </select>
                            @error('kategori')
                                <div class="small text-danger">{{ message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Kategori</label>
                            <select name="subkategori" class="form-control" id="subkategori" required>
                                <option value="" disabled selected hidden>Pilih </option>
                            </select>
                            @error('subkategori')
                                <div class="small text-danger">{{ message }}</div>
                            @enderror
                        </div>

                        <div class="row">
                            <div class="col-lg-4 col-md-12">
                                <div class="form-group">
                                    <div class="form-group">
                                        <input type="file" class="dropify " name="file[]"
                                            data-allowed-file-extensions="png jpg jpeg">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-12">
                                <div class="form-group">
                                    <div class="form-group">
                                        <input type="file" class="dropify " name="file[]"
                                            data-allowed-file-extensions="png jpg jpeg">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-12">
                                <div class="form-group">
                                    <div class="form-group">
                                        <input type="file" class="dropify " name="file[]"
                                            data-allowed-file-extensions="png jpg jpeg">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-12">
                                <div class="form-group">
                                    <div class="form-group">
                                        <input type="file" class="dropify " name="file[]"
                                            data-allowed-file-extensions="png jpg jpeg">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-12">
                                <div class="form-group">
                                    <div class="form-group">
                                        <input type="file" class="dropify " name="file[]"
                                            data-allowed-file-extensions="png jpg jpeg">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-12">
                                <div class="form-group">
                                    <div class="form-group">
                                        <input type="file" class="dropify " name="file[]"
                                            data-allowed-file-extensions="png jpg jpeg">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-12">
                                <div class="form-group">
                                    <div class="form-group">
                                        <input type="file" class="dropify " name="file[]"
                                            data-allowed-file-extensions="png jpg jpeg">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-12">
                                <div class="form-group">
                                    <div class="form-group">
                                        <input type="file" class="dropify " name="file[]"
                                            data-allowed-file-extensions="png jpg jpeg">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-12">
                                <div class="form-group">
                                    <div class="form-group">
                                        <input type="file" class="dropify " name="file[]"
                                            data-allowed-file-extensions="png jpg jpeg">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-12">
            <div class="card">
                <div class="body">
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-hover js-basic-example dataTable table-custom">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>No</th>
                                        <th>Aplikasi</th>
                                        <th>Kategori</th>
                                        <th>Foto</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data as $i => $val)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            <td>{{ $val->tokategori->nama }}</td>
                                            <td>{{ $val->tosubkategori->subkategori }}</td>
                                            <td>
                                                <img src="{{ route('foto.walpaper', $val->id ?? '') }}" alt="" title=""
                                                    width="200px">
                                            </td>
                                            <td>
                                                <a href="{{ route('delete.walpaper', $val->id) }}"
                                                    class="btn btn-danger btn-sm">Hapus</a>
                                            </td>
                                        </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

