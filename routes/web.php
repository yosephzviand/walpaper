<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/kategori', 'Kategori@index')->name('index.kategori');
Route::get('/kategori/delete/{id}', 'Kategori@delete')->name('delete.kategori');
Route::post('/kategori', 'Kategori@store')->name('store.kategori');

Route::get('/walpaper', 'Walpaper@index')->name('index.walpaper');
Route::get('/walpaper/foto/{id}', 'Walpaper@foto')->name('foto.walpaper');
Route::get('/walpaper/delete/{id}', 'Walpaper@delete')->name('delete.walpaper');
Route::post('/walpaper', 'Walpaper@store')->name('store.walpaper');
Route::post('/walpaper/subkategori', 'Walpaper@kategori')->name('kategori.walpaper');

Route::get('/subkategori', 'Subkategori@index')->name('index.subkategori');
Route::get('/subkategori/delete/{id}', 'Subkategori@delete')->name('delete.subkategori');
Route::get('/subkategori/foto/{id}', 'Subkategori@foto')->name('foto.subkategori');
Route::post('/subkategori', 'Subkategori@store')->name('store.subkategori');
